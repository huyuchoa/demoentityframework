﻿using DataLayer.Context;
using Microsoft.Extensions.Logging;
using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reponsitories
{
    public class ReponsitoryBase<T>
    {
        public readonly StudentsDBcontext _context;

        protected readonly ILogger<T> _logger;
        public ReponsitoryBase(StudentsDBcontext context, ILogger<T> logger)
        {
            _context = context;
            _logger = logger;
        }
    }
}
