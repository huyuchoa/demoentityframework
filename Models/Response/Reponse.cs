﻿using Grpc.Core;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Response
{
    public class Reponse<T>
    {
        public Reponse() 
        {
            Code = StatusCodes.Status200OK;
        }
        public int Code { get; set; }
        public string Message { get; set; }

        public T Data { get; set; }
        public List<T> DataList { get; set; }
    }
}
