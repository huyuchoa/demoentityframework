﻿using Microsoft.AspNetCore.Http;
using Models.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Response
{
    public class TableResponse<T>
    {
        public TableResponse() 
        {
            Code = StatusCodes.Status200OK;
            RecordsTatol = RecordsFiltered = 0 ;
            Data = new List<T>();
        }

        public int Code { get; set; }
        public bool Success { get; set;}
        public string Message { get; set;}
        public string DataError { get; set;}
        public int Draw { get; set; }
        public int RecordsTatol { get; set;}
        public int RecordsFiltered { get; set;}
        public List<T> Data { get; set; }

        public void setDraw (TableSearchModel search)
        {
            Draw = search.Draw;
        }
    }
}
