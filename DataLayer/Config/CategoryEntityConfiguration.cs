﻿using DataLayer.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Config
{
    public class CategoryEntityConfiguration : IEntityTypeConfiguration<Category>
    {
      public  void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.ToTable("Categories");
            builder.HasKey(c=> c.Id);
            builder.Property(c =>c.IsActived).HasDefaultValue(true);
            builder.Property(c =>c.IsDeleted).HasDefaultValue(false);
            builder.Property(c=>c.CreateTime).HasDefaultValue(DateTime.Now);
            builder.Property(c => c.UpdateTime).HasDefaultValue(DateTime.Now);
         
        }
    }
}
