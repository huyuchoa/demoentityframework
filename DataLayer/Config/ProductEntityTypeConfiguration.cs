﻿using DataLayer.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Config
{
    public class ProductEntityTypeConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Product> builder)
        {
            builder.ToTable("Products");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.IsActived).HasDefaultValue(true);
            builder.Property(x => x.IsDeleted).HasDefaultValue  (false);
            builder.Property(x => x.UpdateTime).HasDefaultValue(DateTime.Now);
            builder.Property(x => x.CreateTime).HasDefaultValue(DateTime.Now);

        }
    }
}
