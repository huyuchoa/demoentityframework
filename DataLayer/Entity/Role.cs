﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Entity
{
    public class Role: IdentityRole<Guid>
    {
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }
}
