﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Entity
{
    public class Category : Base
    {
        public string CategoryName { get; set; }

        public Product Product { get; set; }
    }
}
