﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Entity
{
    public class Product: Base
    {
        public string ProductName { get; set; }

        public Guid CategoryID { get; set; }
      
        public Category Category { get; set; }
    }
}
